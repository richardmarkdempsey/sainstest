package com.rmd.sains.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rmd.sains.parser.PageParser;
import lombok.Builder;
import lombok.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.math.BigDecimal;

@Data
@Builder
public class Product {
    String title;
    String description;
    @JsonProperty("unit_price")
    BigDecimal unitPrice;
    @JsonProperty("kcal_per_100g")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Integer calories;

    public static Product parse( Element element ){

        Product product = null;
        try {

            Document doc = Jsoup.connect(getProductLink(element)).get();

            product = Product.builder()
                    .title(PageParser.parseProductName(element))
                    .description( PageParser.parseDescription(doc))
                    .unitPrice(PageParser.parseUnitPrice(doc))
                    .calories(PageParser.parseCalories(doc))
                    .build();

        }
        catch( Exception ex ){
            ex.printStackTrace();
            product = null;
        }

        return product;
    }

    private static String getProductLink( Element element ){

        String baseUrl = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries";
        Element link = element.select("a").first();

        String relUrl = link.attr("href");

        return baseUrl + relUrl.substring(relUrl.indexOf("/groceries") + "/groceries".length());
    }
}

