package com.rmd.sains.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Total {

    private BigDecimal gross;

    public Total( BigDecimal gross ){
        this.gross = gross;
    }

    public BigDecimal getGross(){
        return gross;
    }

    public BigDecimal getVat(){
        return gross.subtract(gross.divide(BigDecimal.valueOf(1.2),2, RoundingMode.CEILING));
    }
}
