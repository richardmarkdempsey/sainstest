package com.rmd.sains.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

public class ProductSummary {

    public ProductSummary( List<Product> products ){
        this.results = products;

        totals = new Total(products.stream()
                .map(product->product.getUnitPrice())
                .reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    @Getter
    public List<Product> results;

    @Getter
    @JsonProperty("total")
    public Total totals;
}
