package com.rmd.sains;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rmd.sains.parser.PageParser;
import com.rmd.sains.model.Product;
import com.rmd.sains.model.ProductSummary;
import org.jsoup.Jsoup;

import java.util.stream.Collectors;

public class Application {

    private static final String SAINSBURYS_TEST_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";


    public static void main(String[] args) {

        try {

            ProductSummary productSummary = new ProductSummary(
                        PageParser.parseProducts(Jsoup.connect(SAINSBURYS_TEST_URL).get())
                    .stream()
                    .map(element->Product.parse(element))
                    .filter(product-> product!=null)
                    .collect(Collectors.toList()) );

            ObjectMapper objectMapper = new ObjectMapper();

            String jsonResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(productSummary);

            System.out.println(jsonResult);
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
    }

}
