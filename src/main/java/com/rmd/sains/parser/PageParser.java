package com.rmd.sains.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.math.BigDecimal;

public class PageParser {

    public static Elements parseProducts( Document doc ) throws Exception{

        return doc.select("div.product");
    }

    public static String parseDescription( Document doc ) throws Exception{

        return doc.select("div.productText")
                .first().select("p")
                .first().text().trim();
    }

    public static BigDecimal parseUnitPrice(Document doc ) throws Exception{

        String unitPrice =  doc.select("p.pricePerUnit")
                .first().text().trim().substring(1);

        return new BigDecimal(unitPrice.substring(0, unitPrice.indexOf("/unit")));
    }

    public static Integer parseCalories(Document doc ) throws Exception{

        Elements elements = doc.select("td.nutritionLevel1");

        if( !elements.isEmpty() ) {
            String calories = elements.first().text().trim();

            return Integer.valueOf(calories.substring(0, calories.indexOf("kcal")));
        }

        return null;
    }

    public static String parseProductName(Element element ){
        return element.select("a").first().text().trim();
    }
}
