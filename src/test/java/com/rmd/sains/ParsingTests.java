package com.rmd.sains;

import com.rmd.sains.parser.PageParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class ParsingTests {

    @Test
    public void testProductsAreFoundInMainPage() throws Exception{

        Document mainDocument = Jsoup.parse(new File("src/test/resources/main1.html"),"utf-8");
        Elements products = PageParser.parseProducts( mainDocument );

        assertThat(products.size(), is(2));
    }

    @Test
    public void testNameFoundInMainPage() throws Exception{

        Document mainDocument = Jsoup.parse(new File("src/test/resources/main1.html"),"utf-8");
        Elements products = PageParser.parseProducts( mainDocument );
        String name = PageParser.parseProductName( products.first() );

        assertThat(name, is("product1"));
    }

    @Test
    public void testProductDescriptionFoundInLinkedPage() throws Exception{

        Document linkedDocument = Jsoup.parse(new File("src/test/resources/details1.html"),"utf-8");
        String description = PageParser.parseDescription( linkedDocument );

        assertThat(description, is("a product"));
    }

    @Test
    public void testUnitPriceFoundInLinkedPage() throws Exception{

        Document linkedDocument = Jsoup.parse(new File("src/test/resources/details1.html"),"utf-8");
        BigDecimal unitPrice = PageParser.parseUnitPrice( linkedDocument );

        assertThat(unitPrice.setScale(2), is(BigDecimal.valueOf(10).setScale(2)));
    }

    @Test
    public void testCaloriesFoundInLinkedPage() throws Exception{

        Document linkedDocument = Jsoup.parse(new File("src/test/resources/details1.html"),"utf-8");
        Integer calories = PageParser.parseCalories( linkedDocument );

        assertThat(calories, is(10));
    }

    @Test
    public void testNoCaloriesFoundInLinkedPage() throws Exception{

        Document linkedDocument = Jsoup.parse(new File("src/test/resources/details2.html"),"utf-8");
        Integer calories = PageParser.parseCalories( linkedDocument );

        assertThat( calories, nullValue() );
    }
}
