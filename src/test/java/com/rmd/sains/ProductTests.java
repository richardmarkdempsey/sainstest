package com.rmd.sains;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rmd.sains.model.Product;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ProductTests {

    @Test
    public void testJSONFormattingTitle() throws Exception{

        Product product = Product.builder()
                .title("a product")
                .build();

        assertThat( toJSON(product), containsString("\"title\":\"a product\""));
    }

    @Test
    public void testJSONFormattingDescription() throws Exception{

        Product product = Product.builder()
                .description("a product")
                .build();

        assertThat( toJSON(product), containsString("\"description\":\"a product\""));
    }

    @Test
    public void testJSONFormattingCalories() throws Exception{

        Product product = Product.builder()
                .calories(100)
                .build();

        assertThat( toJSON(product), containsString("\"kcal_per_100g\":100"));
    }

    @Test
    public void testJSONFormattingCaloriesOmitted() throws Exception{

        Product product = Product.builder().build();

        assertThat( toJSON(product), not(containsString("\"kcal_per_100g\"")));
    }

    @Test
    public void testJSONFormattingUnitPrice() throws Exception{

        Product product = Product.builder()
                .unitPrice(new BigDecimal(100))
                .build();

        assertThat( toJSON(product), containsString("\"unit_price\":100"));
    }

    @Test
    public void testJSONFormattingUnitPriceDecimalPlaces() throws Exception{

        Product product = Product.builder()
                .unitPrice(new BigDecimal(100.11111))
                .build();

        assertThat( toJSON(product), containsString("\"unit_price\":100.11"));
    }

    private String toJSON( Product product ) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.writeValueAsString(product);
    }
}
