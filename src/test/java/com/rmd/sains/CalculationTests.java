package com.rmd.sains;

import com.rmd.sains.model.Product;
import com.rmd.sains.model.ProductSummary;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CalculationTests {

    private ProductSummary summary;

    @Before
    public void setup(){
        List<Product> products = new ArrayList<>();
        products.add( Product.builder().unitPrice(BigDecimal.ONE).build());
        products.add( Product.builder().unitPrice(BigDecimal.ONE).build());
        products.add( Product.builder().unitPrice(BigDecimal.ONE).build());
        products.add( Product.builder().unitPrice(BigDecimal.ONE).build());
        products.add( Product.builder().unitPrice(BigDecimal.ONE).build());

        summary = new ProductSummary(products);
    }

    @Test
    public void testGrossCalculation(){

        assertThat( summary.getTotals().getGross(), is(BigDecimal.valueOf(5)) )  ;
    }

    @Test
    public void testVatCalculation(){

//
//        gross = actual * 1.2
//
//                actual = gross/1.2
//
//                        vat = gross - gross/1.2

        assertThat( summary.getTotals().getVat(), is( BigDecimal.valueOf(0.83)) )  ;
    }
}
