
My solution uses Java 8.

install maven

It depends on:

 JSoup to parse html.
 Lomboc to reduce boilerplate code
 Jackson for JSON utilities

 JUnit, Hamcrest for testing

(extact versions listed in pom.xml)

to run tests
mvn '-Dtest=com.rmd.sains.*Tests' test

to run application
mvn exec:java -Dexec.mainClass="com.rmd.sains.Application"